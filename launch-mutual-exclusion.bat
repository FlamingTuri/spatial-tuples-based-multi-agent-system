@echo off

:: cd to current script directory
cd /D "%~dp0"

setlocal

cd spatial_coordination_patterns

gradlew.bat build mutex

endlocal
