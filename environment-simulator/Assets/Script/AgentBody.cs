﻿using Assets.SpatialTuples;
using SynapsisLibrary;
using System.Collections.Generic;

public class AgentBody : SpatialAgentBody
{
    public override void CounterpartEntityReady()
    {
        SynapsisLog("mind is ready");
    }

    public override void CounterpartEntityUnready()
    {
        SynapsisLog("mind is not ready");
    }
}
