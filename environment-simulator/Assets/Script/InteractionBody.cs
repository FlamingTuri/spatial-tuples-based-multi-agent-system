﻿using SynapsisLibrary;
using System.Collections.Generic;

public class InteractionBody : SynapsisBody
{
    public void Start()
    {
        Reset();
    }

    public override void IncomingAction(string action, List<object> parameters)
    {
        SynapsisLog("incoming action: " + action);
        parameters.ForEach(p => SynapsisLog("param: " + p.ToString()));
    }

    public override void CounterpartEntityReady()
    {
        SynapsisLog("mind is ready");
        ReleasedPerception(MindInfo.EntityName, "ready player one");
        TransmitPerception(MindInfo.EntityName, "rasdi", new List<object>() { "pazzi" });
    }

    public override void CounterpartEntityUnready()
    {
        SynapsisLog("mind is not ready");
    }
}
