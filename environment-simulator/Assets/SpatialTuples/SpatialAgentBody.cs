﻿using Assets.SpatialTuples.Conditions;
using Assets.SpatialTuples.Models;
using SynapsisLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Wrld;
using Wrld.Space;

namespace Assets.SpatialTuples
{
    public class SpatialAgentBody : SynapsisBody, ISpatialAgentBody
    {
        private ActionInfo currentActionInfo;
        private Coroutine currentCoroutine;

        private readonly List<SpatialRegion> currentRegions = new List<SpatialRegion>();

        public override void CounterpartEntityReady()
        {
            Debug.Log("Counterpart entity is ready");
        }

        public override void CounterpartEntityUnready()
        {
            Debug.Log("Counterpart entity is destroyed");
        }

        #region Action management
        /// <summary>
        /// Intercepts all incoming actions.
        /// </summary>
        /// <param name="action">The action name</param>
        /// <param name="parameters">The action parameters</param>
        public override void IncomingAction(string action, List<object> parameters)
        {
            Debug.Log($"Action requested: {action}");

            switch (action)
            {
                case Constants.Actions.CREATE_REGION:

                    if (parameters.Count == 3)
                    {
                        var details = parameters[0] as string;
                        var shape = (SpatialRegionShape)int.Parse(parameters[1].ToString());
                        var dimension = float.Parse(parameters[2].ToString());

                        var regionId = CreateRegion(details, shape, dimension, out SpatialRegion spatialRegion);

                        if (!string.IsNullOrEmpty(regionId))
                        {
                            //add newly created region to the current regions
                            currentRegions.Add(spatialRegion);

                            TransmitPerception(MindInfo.EntityName, Constants.Perceptions.REGION_CREATED, regionId);
                        } else
                        {
                            TransmitPerception(MindInfo.EntityName, Constants.Perceptions.REGION_CREATION_FAILED);
                        }

                    } else
                    {
                        Debug.LogWarning($"Skipped {action} execution: parameters are malformed");
                    }

                    break;
                case Constants.Actions.GET_CURRENT_POSITION:

                    var position = GetPosition();
                    if (position != null)
                    {
                        TransmitPerception(MindInfo.EntityName, Constants.Perceptions.CURRENT_POSITION, position.Latitude, position.Longitude);
                    } else
                    {
                        Debug.LogWarning("Unable to get current position");
                    }

                    break;
                case Constants.Actions.GET_CURRENT_REGIONS:

                    TransmitPerception(MindInfo.EntityName, Constants.Perceptions.CURRENT_REGIONS, currentRegions.Select(i => i.name));

                    break;
                case Constants.Actions.GOTO_POSITION:

                    if (parameters.Count == 2)
                    {
                        GoToPosition(new SpatialPosition(double.Parse(parameters[0].ToString()), double.Parse(parameters[1].ToString())));
                    }
                    else
                    {
                        Debug.LogWarning($"Skipped {action} execution: parameters are malformed");
                    }

                    break;
                case Constants.Actions.GOTO_REGION:

                    if (parameters.Count == 1)
                    {
                        GoToRegion(parameters[0].ToString());
                    }
                    else if (parameters.Count == 2)
                    {
                        GoToRegion(parameters[0].ToString(), bool.Parse(parameters[1].ToString()));
                    } else
                    {
                        Debug.LogWarning($"Skipped {action} execution: parameters are malformed");
                    }

                    break;
                case Constants.Actions.LEAVE_REGIONS:

                    List<string> regionNames = new List<string>();

                    if (parameters.Count > 0)
                    {
                        regionNames.AddRange(parameters.Select(i => i.ToString()));
                    } else
                    {
                        regionNames.AddRange(currentRegions.Select(i => i.ObjectName));
                    }

                    LeaveRegions(regionNames.ToArray());

                    break;
                case Constants.Actions.MOVE:

                    if (parameters.Count == 1)
                    {
                        Move(float.Parse(parameters[0].ToString()));
                    }
                    else if (parameters.Count == 2)
                    {
                        var delta = float.Parse(parameters[0].ToString());
                        var steeringAngle = int.Parse(parameters[1].ToString());

                        Move(delta, steeringAngle);                
                    } else
                    {
                        Debug.LogWarning($"Skipped {action} execution: parameters are malformed");
                    }

                    break;
                default:
                    Debug.LogWarning($"Skipped execution of a requested unkown action: {action}");
                    break;
            }
        }
        #endregion

        #region Collision handling
        /// <summary>
        /// On trigger enter event.
        /// </summary>
        /// <param name="other">The collider</param>
        public override void OnTriggerEnter(Collider other)
        {
            string entityName = other.gameObject.name;

            //if the object is not a region ignore
            if (!entityName.StartsWith(Constants.REGION_NAME_PREFIX))
            {
                //TODO: avoid agent overlapping
                return;
            }

            //if an action is pending
            if ((currentActionInfo?.TestStopCondition(other, StopConditionSituation.TriggerEnter)).GetValueOrDefault(false))
            {
                Debug.Log($"[OnTriggerEnter] Current action '{currentActionInfo.ActionName}' stopped (stop condition satisfied)");

                currentActionInfo.PostExecuteAction(other, StopConditionSituation.TriggerEnter);
                StopCurrentAction();
            }
            else
            {
                TransmitPerception(MindInfo.EntityName, Constants.Perceptions.ENTERED_REGION, entityName);
            }

            //update current regions
            var region = other.gameObject.GetComponent<SpatialRegion>();
            if (region != null)
            {
                if (!currentRegions.Contains(region))
                {
                    currentRegions.Add(region);
                }
            }         
        }

        /// <summary>
        /// On trigger stay event.
        /// </summary>
        /// <param name="other">The collider</param>
        private void OnTriggerStay(Collider other)
        {
            string entityName = other.gameObject.name;

            //if the object is not a region ignore
            if (!entityName.StartsWith(Constants.REGION_NAME_PREFIX))
            {
                //TODO: avoid agent overlapping
                return;
            }

            //if an action is pending
            if ((currentActionInfo?.TestStopCondition(other, StopConditionSituation.TriggerStay)).GetValueOrDefault(false))
            {
                Debug.Log($"[OnTriggerStay] Current action '{currentActionInfo.ActionName}' stopped (stop condition satisfied)");

                currentActionInfo.PostExecuteAction(other, StopConditionSituation.TriggerStay);
                StopCurrentAction();
            }
        }

        /// <summary>
        /// On trigger exit event.
        /// </summary>
        /// <param name="other">The collider</param>
        void OnTriggerExit(Collider other)
        {
            string entityName = other.gameObject.name;

            //if the object is not a region ignore
            if (!entityName.StartsWith(Constants.REGION_NAME_PREFIX))
            {
                return;
            }

            //if an action is pending
            if ((currentActionInfo?.TestStopCondition(other, StopConditionSituation.TriggerExit)).GetValueOrDefault(false))
            {
                Debug.Log($"[OnTriggerExit] Current action '{currentActionInfo.ActionName}' stopped (stop condition satisfied)");

                currentActionInfo.PostExecuteAction(other, StopConditionSituation.TriggerExit);
                StopCurrentAction();
            }
            else
            {
                TransmitPerception(MindInfo.EntityName, Constants.Perceptions.LEFT_REGION, entityName);
            }

            //update current regions
            var region = other.gameObject.GetComponent<SpatialRegion>();
            if (region != null)
            {
                currentRegions.Remove(region);
            }
        }
        #endregion

        #region Agent actions
        public string CreateRegion(string details, SpatialRegionShape shape, float dimension, out SpatialRegion spatialRegion)
        {
            spatialRegion = null;

            StopCurrentAction();

            var newObjectId = CreateRegionObjectId(details, shape, dimension);

            if (GameObject.Find(newObjectId) != null)
            {
                Debug.LogError("Cannot create new region: region object id is not unique");
                return string.Empty;
            }

            //if the region to create overlaps completely with another region that it's already present just return the id of the already present region
            var overlappingRegions = GetExistingRegions().Where(i =>
            {
                var info = i.GetComponent<SpatialRegion>();
                return (info != null) ? info.Dimension == dimension && info.Shape == shape && info.gameObject.transform.position == this.transform.position : false;
            });

            if (overlappingRegions.Count() > 0)
            {
                Debug.LogWarning("Another existing region has the same position, shape and dimension: the new region won't be created!");
                return overlappingRegions.First().name;
            }

            //create object
            GameObject gameObject;

            switch (shape)
            {
                case SpatialRegionShape.Cube:
                    gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    break;
                case SpatialRegionShape.Sphere:
                    gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);            
                    break;
                default:
                    Debug.LogError($"Cannot create region because the shape is unknown: {shape}");
                    return string.Empty;
            }

            //set properties
            gameObject.GetComponent<Renderer>().material = CreateRegionMaterial();

            gameObject.name = newObjectId;
            gameObject.tag = Constants.SPATIAL_REGION_TAG;     
            gameObject.transform.position = this.transform.position;
            gameObject.transform.rotation = this.transform.rotation;
            gameObject.transform.localScale = new Vector3(dimension, dimension, dimension);

            //append region properties
            gameObject.AddComponent(typeof(SpatialRegion));
            var region = gameObject.GetComponent<SpatialRegion>();
            region.ObjectName = newObjectId;
            region.Dimension = dimension;
            region.Name = details;
            region.Shape = shape;

            spatialRegion = region;

            Debug.Log($"New region created: {newObjectId}");

            return newObjectId;
        }

        public List<SpatialRegion> GetCurrentRegions()
        {
            return this.currentRegions;
        }

        public SpatialPosition GetPosition()
        {
            var geoPosition = this.gameObject.GetComponent<GeographicTransform>();
            if (geoPosition.TryGetLatLongAltitude(out LatLongAltitude latLongAltitude))
            {
                return new SpatialPosition(latLongAltitude);
            }

            return null;
        }

        public void GoToPosition(SpatialPosition spatialPosition)
        {
            StopCurrentAction();

            var geographicTransform = GetComponentInParent<GeographicTransform>();
            if (geographicTransform != null)
            {
                if (geographicTransform.TryGetLatLongAltitude(out LatLongAltitude latLongAltitude)) {

                    var latLong = spatialPosition.GetLatLongAltitude(latLongAltitude.GetAltitude());

                    var target = Api.Instance.SpacesApi.GeographicToWorldPoint(latLong);
                    if (target != null)
                    {
                        currentActionInfo = new ActionInfo(Constants.Actions.GOTO_POSITION);

                        Debug.Log($"Starting moving to position: {target}");

                        GoToPosition(target, () => TransmitPerception(MindInfo.EntityName, Constants.Perceptions.ARRIVED_TO_POSITION, latLong.GetLatitude(), latLong.GetLongitude()));
                    }
                    else
                    {
                        Debug.LogError("Unable to execute go to position: unable to get screen point");
                    }
                } else
                {
                    Debug.LogError("Unable to execute go to position: unable to get current geographic location");
                }
            }
            else
            {
                Debug.LogError("Unable to execute go to position: the parent do not have geographicsTransform component!");
            }
        }

        public void GoToRegion(string regionName, bool stayOutside = false)
        {
            StopCurrentAction();

            var target = GameObject.Find(regionName);
            if (target != null)
            {
                Debug.Log($"Starting moving to region: {regionName} (stay outside flag: {stayOutside})");

                if (stayOutside)
                {
                    currentActionInfo = new ActionInfo(Constants.Actions.GOTO_REGION)
                    {
                        StopCondition = new StopCondition(c => c.gameObject.name.Equals(regionName), StopConditionSituation.TriggerEnter),
                        PostExecuteAction = (collider, situation) => TransmitPerception(MindInfo.EntityName, Constants.Perceptions.ARRIVED_NEAR_REGION, regionName)
                    };
                } else
                {
                    currentActionInfo = new ActionInfo(Constants.Actions.GOTO_REGION)
                    {
                        StopCondition = new StopCondition(regionCollider => {

                            if (this.GetComponent<Collider>() is Collider agentCollider) {

                                //check if the center position is the same
                                var samePositon = regionCollider.transform.position.Equals(this.transform.position);

                                //check if the region completely contains the entity
                                var regionContainsAgent = false;
                                if (regionCollider is BoxCollider)
                                {
                                    //if the region is a cude, just check that the bounds max and min value are inside the box bounds.
                                    regionContainsAgent = regionCollider.bounds.ContainBounds(agentCollider.bounds);

                                } else if (regionCollider is SphereCollider)
                                {
                                    //if the region is a sphere, shrink region collider bounds.
                                    regionContainsAgent = regionCollider.bounds.ContainBounds(agentCollider.bounds, 0.8f);
                                } else
                                {
                                    Debug.LogWarning("Unable to determine whether the region completely contains the entity: no supported collider found");
                                }

                                return regionCollider.gameObject.name.Equals(regionName) && (samePositon || regionContainsAgent);
                            } else
                            {
                                Debug.LogError("Unable to determine if the region contains the entity: the entity doesn't have a collider.");
                            }

                            return false;
                           
                        }, StopConditionSituation.TriggerStay),
                        PostExecuteAction = (collider, situation) => TransmitPerception(MindInfo.EntityName, Constants.Perceptions.ARRIVED_INSIDE_REGION, regionName)
                    };
                }
               
                GoToPosition(target.transform.position);         
            } else
            {
                Debug.LogError("Unable to execute go to region: region not found");
            }
        }

        public void LeaveRegions(params string[] regions)
        {
            //get all game objects
            var regionObjects = regions.Select(i => GameObject.Find(i)).Where(i => i != null).ToArray();

            if (regions.Length == 0)
            {
                Debug.LogWarning("Unable to execute leave region: no region object found!");
                return;
            }

            StopCurrentAction();

            //compute the total bounding box
            Bounds totalBounds = ComputeTotalBoundingBox(regionObjects);

            //expand the bounding box approximately of the size of this entity
            var agentSize = this.GetComponent<Collider>().bounds.size;
            totalBounds.Expand(agentSize);

            //get the closest point to this entity
            var closestPoint = Vector3.zero;

            var diff = totalBounds.center - this.transform.position;
            if (diff == Vector3.zero) 
            {
                //if the entity is at the center of the bounding box choose one direction
                closestPoint = totalBounds.center + new Vector3(totalBounds.size.x / 2, 0, 0);

            } else 
            {
                //determine the closest border and go in the same direction
                var segment = totalBounds.center - this.transform.position;
                segment.Normalize();

                var ray = new Ray(this.transform.position, segment);
                if (totalBounds.IntersectRay(ray, out float distance))
                {
                    closestPoint = ray.origin + ray.direction * distance; 
                }
            }

            if (closestPoint == Vector3.zero)
            {
                Debug.LogError("Unable to execute leave region action: the casted ray do not intersect with the total bounding box, unable to determine the outer point!");
                return;
            }

            currentActionInfo = new ActionInfo(Constants.Actions.LEAVE_REGIONS);

            Debug.Log($"Starting leaving regions: {string.Join(",", regions)}");

            //move this entity towards the point
            GoToPosition(closestPoint, () => TransmitPerception(MindInfo.EntityName, Constants.Perceptions.EXITED_FROM_REGIONS, regions));
        }

        public void Move(float distance, int steeringAngleDegrees = 0)
        {
            var currRotation = Quaternion.Euler(Vector3.up * steeringAngleDegrees) * this.transform.rotation;     
            var target = currRotation * (Vector3.forward * distance);
            target += transform.position;

            currentActionInfo = new ActionInfo(Constants.Actions.MOVE);

            Debug.Log($"Starting moving forward towards: {target}");

            //move this entity towards the point
            GoToPosition(target, () => TransmitPerception(MindInfo.EntityName, Constants.Perceptions.MOVE_COMPLETED));
        }

        #endregion

        #region Helpers
        /// <summary>
        /// Makes the agent to move into a specific position.
        /// </summary>
        /// <param name="target">The target position</param>
        /// <param name="postExecuteAction">An action that will be invoked when the position is reached</param>
        private void GoToPosition(Vector3 target, Action postExecuteAction = null)
        {   
            currentCoroutine = StartCoroutine(MoveTowards(this.transform.position, target, postExecuteAction));
        }

        /// <summary>
        /// Moves the current this instante from a position to another.
        /// </summary>
        /// <param name="start">The starting position</param>
        /// <param name="end">The ending position</param>
        /// <param name="postExecuteAction">An action that will be invoked when the position is reached</param>
        /// <returns>An enumerator to signal state at each game loop</returns>
        private IEnumerator MoveTowards(Vector3 start, Vector3 end, Action postExecuteAction = null)
        {
            Vector3 dir = (end - start).normalized;
            Quaternion rotTo = Quaternion.LookRotation(dir);

            while (Vector3.Angle(transform.forward, dir) > 1)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rotTo, Time.deltaTime * MovementSpeed * 10);
                yield return new WaitForFixedUpdate(); // Leave the routine and return here in the next frame
            }

            float step = (MovementSpeed / (start - end).magnitude) * Time.fixedDeltaTime;
            float t = 0;
            while (t <= 1.0f)
            {
                t += step; // Goes from 0 to 1, incrementing by step each time
                transform.position = Vector3.Lerp(start, end, t); // Move objectToMove closer to b
                yield return new WaitForFixedUpdate(); // Leave the routine and return here in the next frame
            }

            transform.position = end;

            //invoke post execute action if available
            postExecuteAction?.Invoke();
        }

        /// <summary>
        /// Creates the region game object id.
        /// </summary>
        /// <param name="regionName">The region name</param>
        /// <param name="shape">The shape</param>
        /// <param name="dimension">The dimension</param>
        /// <returns>The identifier</returns>
        private string CreateRegionObjectId(string regionName, SpatialRegionShape shape, double dimension)
        {
            return string.Format("{0}-{1}-{2}-{3}", Constants.REGION_NAME_PREFIX, regionName, shape.ToString(), dimension);
        }

        /// <summary>
        /// Stops the current executing action.
        /// </summary>
        private void StopCurrentAction()
        {
            if (currentCoroutine != null)
            {
                StopCoroutine(currentCoroutine);
                currentActionInfo = null;
                currentCoroutine = null;
            }           
        }

        /// <summary>
        /// Computes the total bounding box that contains all input objects.
        /// </summary>
        /// <param name="gameObjects">the game objects</param>
        /// <returns>The total bounding box</returns>
        private Bounds ComputeTotalBoundingBox(params GameObject[] gameObjects)
        {
            var combinedBounds = GetComponent<Renderer>().bounds;
            foreach (var obj in gameObjects)
            {
                if (obj != null)
                {
                    var rendered = obj.GetComponent<Renderer>();
                    if (rendered != null)
                    {
                        combinedBounds.Encapsulate(rendered.bounds);
                    }
                }
            }

            return combinedBounds;
        }

        /// <summary>
        /// Creates the material of a spatial region.
        /// </summary>
        /// <returns>The material of the region</returns>
        private Material CreateRegionMaterial()
        {
            var material = Resources.Load<Material>("Material/SpatialRegion");

            if (material == null)
            {
                var color = Color.green;
                color.a = 0.6f;

                Material m = new Material(Shader.Find("Standard"));
                m.SetFloat("_Mode", 3);
                m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                m.SetInt("_ZWrite", 0);
                m.DisableKeyword("_ALPHATEST_ON");
                m.EnableKeyword("_ALPHABLEND_ON");
                m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                m.renderQueue = 3000;
                m.color = color;

                return m;
            }

            return material;
        }

        /// <summary>
        /// Gets all GameObjects that represents a region.
        /// </summary>
        /// <returns>The list of game objects that represents a region.</returns>
        private List<GameObject> GetExistingRegions()
        {
            try
            {
                return GameObject.FindGameObjectsWithTag(Constants.SPATIAL_REGION_TAG).Where(i => i.name.StartsWith(Constants.REGION_NAME_PREFIX)).ToList();
            }
            catch (Exception ex)
            {
                Debug.LogError($"The tag does not exists. Create a new tag named '{Constants.SPATIAL_REGION_TAG}' within the editor and assign it to all existing region (Edit > Project Settings > Tags and Layers)");
                throw ex;
            }
        }
        #endregion
    }
}
