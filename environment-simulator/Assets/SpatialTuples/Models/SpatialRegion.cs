﻿using UnityEngine;

namespace Assets.SpatialTuples.Models
{
    /// <summary>
    /// The details about a spatial region.
    /// </summary>
    public class SpatialRegion : Component
    {
        #region Fields
        /// <summary>
        /// The name of the relative game object.
        /// </summary>
        public string ObjectName { get; set; }
        /// <summary>
        /// The region name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The shape of the region.
        /// </summary>
        public SpatialRegionShape Shape { get; set; }
        /// <summary>
        /// The dimension of the region. The radius of the sphere or the length of a cube side.
        /// </summary>
        public float Dimension { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// The default constructor
        /// </summary>
        /// <param name="objectName">The object name</param>
        /// <param name="name">The name</param>
        /// <param name="shape">The shape</param>
        /// <param name="dimension">The dimension</param>
        public SpatialRegion(string objectName, string name, SpatialRegionShape shape, float dimension)
        {
            this.ObjectName = objectName;
            this.Name = name;
            this.Shape = shape;
            this.Dimension = dimension;
        }

        /// <summary>
        /// The secondary constructor
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="shape">The shape</param>
        /// <param name="dimension">The dimension</param>
        public SpatialRegion(string name, SpatialRegionShape shape, float dimension)
        {
            this.Name = name;
            this.Shape = shape;
            this.Dimension = dimension;
        }
        #endregion

        #region Cloning
        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>A new instance filled with the information of this instance</returns>
        public SpatialRegion Clone()
        {
            return new SpatialRegion(this.ObjectName, this.Name, this.Shape, this.Dimension);
        }
        #endregion
    }
}
