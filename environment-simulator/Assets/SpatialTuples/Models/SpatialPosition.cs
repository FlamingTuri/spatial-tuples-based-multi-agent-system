﻿using Wrld.Space;

namespace Assets.SpatialTuples.Models
{
    /// <summary>
    /// Represents a spatial position in geographic coordinates.
    /// </summary>
    public class SpatialPosition
    {
        #region Fields
        /// <summary>
        /// The latitude.
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// The longitude.
        /// </summary>
        public double Longitude { get; set; }
        #endregion
        
        #region Constructors
        /// <summary>
        /// The default constructor.
        /// </summary>
        public SpatialPosition()
        {
            Latitude = 0;
            Longitude = 0;
        }

        /// <summary>
        /// The secondary constructor.
        /// </summary>
        /// <param name="latitude">The latitude</param>
        /// <param name="longitude">The longitude</param>
        public SpatialPosition(double latitude, double longitude) : base()
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        /// <summary>
        /// The secondary constructor.
        /// </summary>
        /// <param name="latLong">The LatLong object</param>
        public SpatialPosition(LatLong latLong) : this(latLong.GetLatitude(), latLong.GetLongitude()) { }

        /// <summary>
        /// The secondary constructor.
        /// </summary>
        /// <param name="latLong">The LatLongAltitude object</param>
        public SpatialPosition(LatLongAltitude latLongAltitude) : this(latLongAltitude.GetLatLong()) { }
        #endregion

        #region Helpers
        /// <summary>
        /// Gets the LatLong object that represents the coordinates of this instance.
        /// </summary>
        /// <returns>The LatLong object</returns>
        public LatLong GetLatLong()
        {
            return new LatLong(Latitude, Longitude);
        }

        /// <summary>
        /// Gets the LatLongAltitude object that represents the coordinates of this instance.
        /// </summary>
        /// <param name="originalAltitude">The original altitude (default: 0)</param>
        /// <returns>The LatLongAltitude object</returns>
        public LatLongAltitude GetLatLongAltitude(double originalAltitude = 0)
        {
            return new LatLongAltitude(Latitude, Longitude, originalAltitude);
        }
        #endregion
    }
}
