﻿namespace Assets.SpatialTuples.Models
{
    /// <summary>
    /// Represents the possible spatial region shapes.
    /// </summary>
    public enum SpatialRegionShape : int
    {
        Cube,
        Sphere
    }
}
