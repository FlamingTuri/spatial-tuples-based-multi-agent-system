﻿namespace Assets.SpatialTuples
{
    class Constants
    {
        public const string REGION_NAME_PREFIX = "region";
        public const string SPATIAL_REGION_TAG = "spatialRegion";

        public class Actions
        {
            public const string CREATE_REGION = "createRegion";
            public const string GET_CURRENT_POSITION = "getCurrentPosition";
            public const string GET_CURRENT_REGIONS = "getCurrentRegions";
            public const string GOTO_POSITION = "gotoPosition";
            public const string GOTO_REGION = "gotoRegion";
            public const string LEAVE_REGIONS = "leaveRegions";
            public const string MOVE = "move";
        }
        
        public class Perceptions
        {
            //success
            public const string CURRENT_POSITION = "currentPosition";
            public const string CURRENT_REGIONS = "currentRegions";

            public const string REGION_CREATED = "regionCreated";         
            public const string ARRIVED_TO_POSITION = "arrivedToPosition";
            public const string ARRIVED_INSIDE_REGION = "arrivedInsideRegion";
            public const string ARRIVED_NEAR_REGION = "arrivedNearRegion";
            public const string MOVE_COMPLETED = "moveCompleted";
            public const string EXITED_FROM_REGIONS = "exitedFromRegions";
            public const string ENTERED_REGION = "enteredRegion";
            public const string LEFT_REGION = "leftRegion";

            //failure
            public const string REGION_CREATION_FAILED = "regionCreationFailed";
        } 
    }
}
