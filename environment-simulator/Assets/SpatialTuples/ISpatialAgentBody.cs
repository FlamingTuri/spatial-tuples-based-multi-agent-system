﻿using Assets.SpatialTuples.Models;
using System.Collections.Generic;

namespace Assets.SpatialTuples
{
    /// <summary>
    /// Interface for a Spatial Agent.
    /// </summary>
    public interface ISpatialAgentBody
    {
        /// <summary>
        /// Creates a new region in the current agent position with a specified shape and dimension.
        /// </summary>
        /// <param name="details">The details</param>
        /// <param name="shape">The shape</param>
        /// <param name="dimension">The dimension</param>
        /// <param name="spatialRegion">The object that contains the information about the newly created region</param>
        /// <returns>The identifier of the new region</returns>
        string CreateRegion(string details, SpatialRegionShape shape, float dimension, out SpatialRegion spatialRegion);

        /// <summary>
        /// Retrieves all the region where the agent is currently in.
        /// </summary>
        /// <returns>The list of regions</returns>
        List<SpatialRegion> GetCurrentRegions();

        /// <summary>
        /// Gets the agent current position.
        /// </summary>
        /// <returns>The position</returns>
        SpatialPosition GetPosition();

        /// <summary>
        /// Signals the agent to go in a specified position.
        /// </summary>
        /// <param name="spatialPosition">The spatial position</param>
        void GoToPosition(SpatialPosition spatialPosition);

        /// <summary>
        /// Signals the agent to inside a specified region.
        /// </summary>
        /// <param name="regionName">The name of the target region.</param>
        /// <param name="stayOutside">The flag to determine whether the agent should stop as soon as it touches the region or go inside the region then stop.</param>
        void GoToRegion(string regionName, bool stayOutside = false);

        /// <summary>
        /// Signals the agent to leave from one or more regions.
        /// </summary>
        /// <param name="regions">The list of regions to leave (if no region is specified, the agent will leave all regions)</param>
        void LeaveRegions(params string[] regions);

        /// <summary>
        /// Forces the agent to move by a specified amount.
        /// </summary>
        /// <param name="distance">The distance</param>
        /// <param name="directionOffset">The direction offset (> 0 is applied to the right, < 0 to the left)</param>
        void Move(float distance, int directionOffset = 0);
    }
}
