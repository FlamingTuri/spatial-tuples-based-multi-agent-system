﻿using System.Linq;
using UnityEngine;

namespace Assets.SpatialTuples
{
    public static class Boundsxtension
    {
        /// <summary>
        /// Determines whether a Bounds object is completely contained into another Bounds.
        /// </summary>
        /// <param name="bounds">The Bounds object</param>
        /// <param name="target">The Bounds object that needs to be tested</param>
        /// <param name="adjust">The adjust value to apply to the outer bounds</param>
        /// <returns>true, if bounds contains target; otherwise false</returns>
        public static bool ContainBounds(this Bounds bounds, Bounds target, float adjust = 1.0f)
        {
            var adjustedBounds = bounds;
            if (adjust != 1.0f)
            {
                adjustedBounds = new Bounds(bounds.center, bounds.size * adjust); 
            }
            return adjustedBounds.Contains(target.min) && adjustedBounds.Contains(target.max);
        }
    }
}
