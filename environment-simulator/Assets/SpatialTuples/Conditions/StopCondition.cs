﻿using System;
using UnityEngine;

namespace Assets.SpatialTuples.Conditions
{
    /// <summary>
    /// Represents the condition when the agent needs to stop.
    /// </summary>
    class StopCondition : IStopCondition
    {
        #region Static
        /// <summary>
        /// Represents a null condition.
        /// </summary>
        public static IStopCondition Null = new NullCondition();
        #endregion

        #region Fields
        /// <summary>
        /// The predicate that represents the condition.
        /// </summary>
        public Predicate<Collider> Predicate { get; }

        /// <summary>
        /// Represents in which situation when the condition needs to be verified.
        /// </summary>
        public StopConditionSituation Situation { get; }
        #endregion

        #region Constructor
        /// <summary>
        /// The default constructor.
        /// </summary>
        /// <param name="predicate">The predicate</param>
        /// <param name="situation">The situation</param>
        public StopCondition(Predicate<Collider> predicate, StopConditionSituation situation = StopConditionSituation.Any)
        {
            this.Predicate = predicate;
            this.Situation = situation;
        }
        #endregion

        public bool Test(Collider collider, StopConditionSituation situation)
        {
            if (situation != StopConditionSituation.Any && situation != this.Situation) { return false; }
            return Predicate.Invoke(collider);
        }
    }
}
