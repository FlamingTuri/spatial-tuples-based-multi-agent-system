﻿using System;
using UnityEngine;

namespace Assets.SpatialTuples.Conditions
{
    /// <summary>
    /// The action information.
    /// </summary>
    class ActionInfo
    {
        #region Fields
        /// <summary>
        /// The action name.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// The stop condition.
        /// </summary>
        public IStopCondition StopCondition { get; set; } = Conditions.StopCondition.Null;

        /// <summary>
        /// The post execute action.
        /// </summary>
        public Action<Collider, StopConditionSituation> PostExecuteAction { get; set; } = (collider, situation) => { };
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ActionInfo()
        {

        }

        /// <summary>
        /// Secondary constructor.
        /// </summary>
        /// <param name="actionName">The action name.</param>
        public ActionInfo(string actionName) : this()
        {
            this.ActionName = actionName;
        }

        /// <summary>
        /// Secondary constructor.
        /// </summary>
        /// <param name="actionName">The action name.</param>
        /// <param name="stopCondition">The condition</param>
        /// <param name="postExecuteAction">The post execute action</param>
        public ActionInfo(string actionName, IStopCondition stopCondition, Action<Collider, StopConditionSituation> postExecuteAction) : this()
        {
            this.ActionName = actionName;
            this.StopCondition = stopCondition;
            this.PostExecuteAction = postExecuteAction;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Tests the action stop condition.
        /// </summary>
        /// <param name="collider">The collider</param>
        /// <param name="situation">The current trigger situation</param>
        /// <returns>True, if the stop condition is satisfied; otherwise false.</returns>
        public bool TestStopCondition(Collider collider, StopConditionSituation situation) => (StopCondition?.Test(collider, situation)).GetValueOrDefault(false);

        /// <summary>
        /// Executes the post action function.
        /// </summary>
        /// <param name="collider">The collider</param>
        /// <param name="situation">The current trigger situation</param>
        public void ExecutePostAction(Collider collider, StopConditionSituation situation) => PostExecuteAction?.Invoke(collider, situation);
        #endregion
    }
}
