﻿using UnityEngine;

namespace Assets.SpatialTuples.Conditions
{
    class NullCondition : IStopCondition
    {
        public bool Test(Collider collider, StopConditionSituation situation)
        {
            return false;
        }
    }
}
