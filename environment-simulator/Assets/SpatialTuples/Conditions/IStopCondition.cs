﻿using UnityEngine;

namespace Assets.SpatialTuples.Conditions
{
    interface IStopCondition
    {
        bool Test(Collider collider, StopConditionSituation situation);
    }
}
