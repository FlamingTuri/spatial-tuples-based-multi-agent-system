﻿namespace Assets.SpatialTuples.Conditions
{
    enum StopConditionSituation
    {
        Any,
        TriggerEnter,
        TriggerStay,
        TriggerExit
    }
}
