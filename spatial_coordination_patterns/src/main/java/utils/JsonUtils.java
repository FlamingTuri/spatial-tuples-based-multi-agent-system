package utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Utility class used to parse JSON strings
 */
public class JsonUtils {

    /**
     * Creates a class instance from the specified JSON string
     *
     * @param targetClass the class that should be instantiated
     * @param consumer    a functional way to customize the object mapper that will parse the JSON string
     * @param jsonString  the JSON string to parse
     * @param <T>         the type of the class that should be instantiated
     * @return the instance of the class derived from the JSON parsing
     * @throws IOException when the parse fails
     */
    public static <T> T fromJsonString(Class<T> targetClass, Consumer<ObjectMapper> consumer, String jsonString) throws IOException {
        var mapper = new ObjectMapper().enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES);
        consumer.accept(mapper);
        return mapper.readValue(jsonString, targetClass);
    }

    public String toJsonString() {
        ObjectMapper mapper = new ObjectMapper();
        var jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString
            .replace("\\", "")
            .replace("\"[", "[")
            .replace("]\"", "]");
    }
}
