package utils;

import java.util.stream.Stream;

/**
 * Logging utility class
 */
public class Logger {

    /**
     * Prints the specified messages
     *
     * @param messages the messages to print
     */
    public static void print(Object... messages) {
        var stackTrace = Thread.currentThread().getStackTrace();
        var caller = stackTrace[2];
        var callerClassName = caller.getClassName();
        int callerLineNumber = caller.getLineNumber();
        Stream.of(messages).map(message -> message.toString()).forEach(message -> {
            var toPrint = String.format("[%s]: %s (line %d)", callerClassName, message, callerLineNumber);
            System.out.println(toPrint);
        });
    }
}
