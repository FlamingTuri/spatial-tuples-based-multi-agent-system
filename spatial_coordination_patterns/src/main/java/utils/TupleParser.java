package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import models.Tuple;

public class TupleParser {
	
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	static public Tuple parseCartagoTupleToTusowTuple(String cartagoTupla) {
		int indexEndFun = cartagoTupla.indexOf("(");
		if (indexEndFun < 1) {
			throw new IllegalArgumentException("no fun found for tupla: " + cartagoTupla);
		}
		if (!cartagoTupla.endsWith(")")) {
			throw new IllegalArgumentException("no final \")\" found for tupla: " + cartagoTupla);
		}
		String fun = cartagoTupla.substring(0, indexEndFun);
		String argsToParse = cartagoTupla.substring(indexEndFun+1, cartagoTupla.length()-1);
		ArrayNode tusowArgs = OBJECT_MAPPER.createArrayNode();

		int firstComma = argsToParse.indexOf(",");
		int firstBracket = argsToParse.indexOf("(");
		
		while ((firstComma != -1 || firstBracket != -1) && (firstComma != 0 && firstBracket != 0)) {
			//parse next arg
			if ((firstComma < firstBracket && firstComma > 0) || firstBracket == -1) {
				if (argsToParse.startsWith("_")) {
					//parse variable
					tusowArgs.add(OBJECT_MAPPER.createObjectNode().put("var", argsToParse.substring(0, firstComma)));
				} else {
					//parse simple arg
					tusowArgs.add(argsToParse.substring(0, firstComma));
				}
				argsToParse = argsToParse.substring(firstComma+1);
			} else {
				//parse tupla
				int indexCloseBracket = searchClosedBracketIndex(argsToParse);
				if (indexCloseBracket == -1 && indexCloseBracket < firstBracket) {
					throw new IllegalArgumentException("invalid \")\" in subTupla for tupla: " + cartagoTupla);
				}
				Tuple subTupla = parseCartagoTupleToTusowTuple(argsToParse.substring(0, indexCloseBracket+1));
				tusowArgs.add(subTupla.asJsonNode());
				argsToParse = argsToParse.substring(indexCloseBracket+1);
				if (argsToParse.startsWith(",")) {
					argsToParse = argsToParse.substring(1);
				}
				firstBracket = argsToParse.indexOf("(");
			}
			firstComma = argsToParse.indexOf(",");
		}
		
		if (firstComma == 0 || firstBracket == 0) {
			throw new IllegalArgumentException("invalid args for tupla: " + cartagoTupla);
		}
		
		if (!argsToParse.isEmpty()) {
			if (argsToParse.startsWith("_")) {
				//parse variable
				tusowArgs.add(OBJECT_MAPPER.createObjectNode().put("var", argsToParse));
			} else {
				//parse simple arg
				tusowArgs.add(argsToParse);
			}
		}
		return new Tuple(fun, tusowArgs);
	}
	
	private static int searchClosedBracketIndex(String tupla) {
		var firstBracket = tupla.indexOf("(");
		var numberOpenBracket = 1;
		int closedBracketIndex;
		for (closedBracketIndex = firstBracket+1; closedBracketIndex < tupla.length() && numberOpenBracket != 0; closedBracketIndex++) {
			if (tupla.charAt(closedBracketIndex) == '(') {
				numberOpenBracket++;
			} else if (tupla.charAt(closedBracketIndex) == ')') {
				numberOpenBracket--;
			}
		}
		
		return numberOpenBracket == 0 ? closedBracketIndex-1 : -1;
	}
}
