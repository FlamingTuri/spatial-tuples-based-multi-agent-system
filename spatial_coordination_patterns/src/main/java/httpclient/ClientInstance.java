package httpclient;

import java.net.http.HttpClient;

public class ClientInstance {

    private static HttpClient client;

    private ClientInstance() {
    }

    public static HttpClient get() {
        if (client == null) {
            client = HttpClient.newHttpClient();
        }
        return client;
    }
}
