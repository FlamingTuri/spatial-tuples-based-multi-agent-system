package httpclient;

public interface HttpMethod {
    public static final String GET = "GET";
    public static final  String POST = "POST";
    public static final String DELETE = "DELETE";
}
