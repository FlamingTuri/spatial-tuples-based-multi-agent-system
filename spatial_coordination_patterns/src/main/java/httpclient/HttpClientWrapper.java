package httpclient;

import queryparams.DeleteQueryParams;
import queryparams.GetQueryParams;
import queryparams.PostQueryParams;
import queryparams.QueryParams;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;

/**
 * Class used to send request to TuSoW service and retrieve it's responses
 */
public class HttpClientWrapper {

    private static final String JSON_MIME_TYPE = "application/json";
    private final HttpClient client;
    private final String tusowAddress;

    public HttpClientWrapper() {
        this("http://localhost:8080");
    }

    public HttpClientWrapper(String tusowAddress) {
        this.client = ClientInstance.get();
        this.tusowAddress = tusowAddress + "/tusow/v1/tuple-spaces/logic/";
    }

    private HttpRequest buildRequest(String method, String tupleSpaceName, String body) {
        return HttpRequest.newBuilder()
            .uri(URI.create(tusowAddress + tupleSpaceName))
            .header("Content-Type", JSON_MIME_TYPE)
            .header("Accept", JSON_MIME_TYPE)
            .method(method, HttpRequest.BodyPublishers.ofString(body))
            .build();
    }

    private CompletableFuture<HttpResponse<String>> sendRequest(String httpMethod, String tupleSpaceName, String body) {
        var request = buildRequest(httpMethod, tupleSpaceName, body);
        return sendRequest(request);
    }

    private CompletableFuture<HttpResponse<String>> sendRequest(HttpRequest request) {
        return client.sendAsync(request, BodyHandlers.ofString());
    }

    private String addQueryParams(String tupleSpaceName, QueryParams queryParams) {
        return tupleSpaceName + queryParams.stringify();
    }

    /**
     * Makes a non blocking get request to TuSoW service at the specified tupleSpaceName
     *
     * @param tupleSpaceName the space name where the tuple will be searched
     * @param body           the tuple template to match
     * @return a CompletableFuture storing the request result
     */
    public CompletableFuture<HttpResponse<String>> get(String tupleSpaceName, String body) {
        return sendRequest(HttpMethod.GET, tupleSpaceName, body);
    }

    /**
     * Makes a non blocking get request to TuSoW service at the specified tupleSpaceName
     *
     * @param tupleSpaceName the space name where the tuple will be searched
     * @param getQueryParams the query params to add to the request
     * @param body           the tuple template to match
     * @return a CompletableFuture storing the request result
     */
    public CompletableFuture<HttpResponse<String>> get(String tupleSpaceName, GetQueryParams getQueryParams, String body) {
        return get(addQueryParams(tupleSpaceName, getQueryParams), body);
    }

    /**
     * Makes a non blocking post request to TuSoW service at the specified tupleSpaceName
     *
     * @param tupleSpaceName the space name where the tuple will be inserted
     * @param body           the tuple template to insert
     * @return a CompletableFuture storing the request result
     */
    public CompletableFuture<HttpResponse<String>> post(String tupleSpaceName, String body) {
        return sendRequest(HttpMethod.POST, tupleSpaceName, body);
    }

    /**
     * Makes a non blocking post request to TuSoW service at the specified tupleSpaceName
     *
     * @param tupleSpaceName  the space name where the tuple will be inserted
     * @param postQueryParams the query params to add to the request
     * @param body            the tuple template to insert
     * @return a CompletableFuture storing the request result
     */
    public CompletableFuture<HttpResponse<String>> post(String tupleSpaceName, PostQueryParams postQueryParams, String body) {
        return post(addQueryParams(tupleSpaceName, postQueryParams), body);
    }

    /**
     * Makes a non blocking delete request to TuSoW service at the specified tupleSpaceName
     *
     * @param tupleSpaceName the space name from which the tuple will be deleted
     * @param body           the tuple template that will be deleted upon matching
     * @return a CompletableFuture storing the request result
     */
    public CompletableFuture<HttpResponse<String>> delete(String tupleSpaceName, String body) {
        return sendRequest(HttpMethod.DELETE, tupleSpaceName, body);
    }

    /**
     * Makes a non blocking delete request to TuSoW service at the specified tupleSpaceName
     *
     * @param tupleSpaceName    the space name from which the tuple will be deleted
     * @param deleteQueryParams the query params to add to the request
     * @param body              the tuple template that will be deleted upon matching
     * @return a CompletableFuture storing the request result
     */
    public CompletableFuture<HttpResponse<String>> delete(String tupleSpaceName, DeleteQueryParams deleteQueryParams, String body) {
        return delete(addQueryParams(tupleSpaceName, deleteQueryParams), body);
    }
}
