package queryparams;

public class GetQueryParams extends DeleteQueryParams {

    private static final String NEGATED = "negated";

    /**
     * Adds the "negated=true" query param
     */
    public void setNegated() {
        addQueryParam(NEGATED);
    }
}
