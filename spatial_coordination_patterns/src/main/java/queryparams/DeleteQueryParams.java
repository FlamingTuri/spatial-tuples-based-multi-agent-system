package queryparams;

public class DeleteQueryParams extends PostQueryParams {

    protected static final String PREDICATIVE = "predicative";

    @Override
    public void setBulk() {
        super.setBulk();
        removeQueryParam(PREDICATIVE);
    }

    /**
     * Adds the "predicative=true" query param
     */
    public void setPredicative() {
        addQueryParam(PREDICATIVE);
        removeQueryParam(BULK);
    }
}
