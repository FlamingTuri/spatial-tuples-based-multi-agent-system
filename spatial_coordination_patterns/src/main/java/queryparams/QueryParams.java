package queryparams;

import jason.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class QueryParams {

    protected List<Pair<String, Boolean>> queryParams = new ArrayList<>();

    /**
     * Adds a query param and sets it's value to true
     *
     * @param key the identifier of the query param
     */
    public void addQueryParam(String key) {
        queryParams.add(new Pair<>(key, true));
    }

    /**
     * Removes the specified query param
     *
     * @param key the identifier of the query param
     */
    public void removeQueryParam(String key) {
        queryParams = queryParams.stream()
            .filter(p -> !p.getFirst().equals(key))
            .collect(Collectors.toList());
    }

    /**
     * Returns the query params
     *
     * @return the query params lis
     */
    public List<Pair<String, Boolean>> getQueryParams() {
        return queryParams;
    }

    /**
     * Returns the url version of the query params
     *
     * @return ?param1.key=param1.value&param2.key=param2.value ...
     */
    public String stringify() {
        var stringBuilder = new StringBuilder();
        stringBuilder.append("?");
        queryParams.forEach(p ->
            stringBuilder.append(p.getFirst())
                .append("=")
                .append(p.getSecond())
                .append("&")
        );
        var result = stringBuilder.toString();
        return result.substring(0, result.length() - 1);
    }
}
