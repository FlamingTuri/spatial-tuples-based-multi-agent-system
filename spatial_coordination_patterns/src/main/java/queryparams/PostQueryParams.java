package queryparams;

public class PostQueryParams extends QueryParams {

    protected static final String BULK = "bulk";

    /**
     * Adds the "bulk=true" query param
     */
    public void setBulk() {
        addQueryParam(BULK);
    }
}
