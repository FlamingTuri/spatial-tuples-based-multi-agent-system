package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ResponseMixIn {

    public ResponseMixIn(@JsonProperty("template") Tuple template,
                         @JsonProperty("tuple") Tuple tuple,
                         @JsonProperty("match") boolean match,
                         @JsonProperty("map") ObjectNode map) {
    }
}
