package models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import utils.JsonUtils;

import java.io.IOException;
import java.util.function.Consumer;

public class Response extends JsonUtils {

    private Tuple template;
    private Tuple tuple;
    private boolean match;
    private ObjectNode map;

    public Response(Tuple template, Tuple tuple, boolean match, ObjectNode map) {
        this.template = template;
        this.tuple = tuple;
        this.match = match;
        this.map = map;
    }

    private static Consumer<ObjectMapper> getConsumer() {
        return objectMapper ->
            objectMapper.addMixIn(Tuple.class, TupleMixIn.class)
                .addMixIn(Response.class, ResponseMixIn.class);
    }

    public static Response fromJsonString(String jsonString) throws IOException {
        return fromJsonString(Response.class, getConsumer(), jsonString);
    }

    public static Response[] fromJsonArrayString(String jsonString) throws IOException {
        return fromJsonString(Response[].class, getConsumer(), jsonString);
    }

    public Tuple getTemplate() {
        return template;
    }

    public void setTemplate(Tuple template) {
        this.template = template;
    }

    public Tuple getTuple() {
        return tuple;
    }

    public void setTuple(Tuple tuple) {
        this.tuple = tuple;
    }

    public boolean isMatch() {
        return match;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }

    public ObjectNode getMap() {
        return map;
    }

    public void setMap(ObjectNode map) {
        this.map = map;
    }
}
