package models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import utils.JsonUtils;

import java.io.IOException;
import java.util.function.Consumer;

public class Tuple extends JsonUtils {

    private String fun;
    private ArrayNode args;

    public Tuple(String fun) {
        this(fun, new ObjectMapper().createArrayNode());
    }

    public Tuple(String fun, ArrayNode args) {
        this.fun = fun;
        this.args = args;
    }

    private static Consumer<ObjectMapper> getConsumer() {
        return objectMapper -> objectMapper.addMixIn(Tuple.class, TupleMixIn.class);
    }

    public static Tuple fromJsonString(String jsonString) throws IOException {
        return fromJsonString(Tuple.class, getConsumer(), jsonString);
    }

    public static Tuple[] fromJsonArrayString(String jsonString) throws IOException {
        return fromJsonString(Tuple[].class, getConsumer(), jsonString);
    }

    public String getFun() {
        return fun;
    }

    public void setFun(String fun) {
        this.fun = fun;
    }

    public ArrayNode getArgs() {
        return args;
    }

    public void setArgs(ArrayNode args) {
        this.args = args;
    }
    
    public JsonNode asJsonNode() {
    	return new ObjectMapper().createObjectNode()
    			.put("fun", this.fun)
    			.put("args", this.args.toString());
    }
}
