package models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Term {

    private static final String VAR = "var";
    private static final String VAL = "val";
    private static final String KEY = "key";
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private Term() {
    }

    private static ObjectNode put(String fieldName, String value) {
        return objectMapper.createObjectNode().put(fieldName, value);
    }

    public static ObjectNode createVarTerm(String identifier) {
        return put(VAR, identifier);
    }

    public static ObjectNode createValTerm(String identifier) {
        return put(VAR, identifier);
    }

    public static ObjectNode createKeyTerm(String identifier) {
        return put(KEY, identifier);
    }

    public static ObjectNode createTerm(String varIdentifier, String valIdentifier) {
        return createVarTerm(varIdentifier).put(VAL, valIdentifier);
    }
}
