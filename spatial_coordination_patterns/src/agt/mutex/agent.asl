// Agent sample_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
/*tusow */
tusow_url("http://localhost:8080").
tupleSpaceName("mutex").
region("region-mutex-Sphere-80").
verbose(true).
/*synapsis */
synapsis_url("ws://localhost:9000/").
synapsis_mind_class("mutex.AgentMind").
reconnection_attempts(5).

pendingJob(1).
/* Initial goals */

!start.

/* Plans */

+arrivedNear(R) : region(Reg) & R == Reg <-
	.my_name(ID);
	.print(ID, "arrived near region");
	!in(lock(mutex), [R]);
	!goInsideRegion.
	
+arrivedInside(R) : region(Reg) & R == Reg <-
	.my_name(ID);
	.print(ID, "arrived inside region");
	.print("do my job");
	.wait(1000);
	.print("end my job, I'll go out the mutex region")
	!leaveRegion.
	
+exitFrom(R) : region(Reg) & R == Reg <-
	.my_name(ID);
	!outR(lock(mutex), R);
	.print(ID, "lock released").

+!start : verbose(V) <- 
    !startClient(V);
    !createSynapsisMind([]).

+!start : true <-
	+verbose(false);
	!start.
	
+synapsis_counterpart_status(Name, C): pendingJob(N) & C & .my_name(Me) & .substring(Me,Name) <-
    !log("Body connected");
    if (N == 1) {
    	-+pendingJob(0)
    	!goNearRegion;
    }.
    
+synapsis_counterpart_status(Name, C): not C & .my_name(Me) & .substring(Me,Name) <-
    .drop_all_intentions;
    !log("Body not connected").
	
+!goNearRegion : region(R) <-
	.print("go near region: ", R);
	?my_synapsis_mind_ID(MyArtID);
	goNearRegion(R)[artifact_id(MyArtID)].
	
+!goInsideRegion : region(R) <-
	.print("go inside region: ", R);
	?my_synapsis_mind_ID(MyArtID);
	goInsideRegion(R)[artifact_id(MyArtID)].
	
+!leaveRegion : region(R) <-
	.print("leave region: ", R);
	?my_synapsis_mind_ID(MyArtID);
	leaveRegion(R)[artifact_id(MyArtID)].

{ include("tusow_agent.asl")}
{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl")}