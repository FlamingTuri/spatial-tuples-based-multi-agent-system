// Agent sample_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
tusow_url("http://localhost:8080").
tupleSpaceName("mutex").
expetedDevice(5).
region("region-mutex-Sphere-80").
verbose(true).

/* Initial goals */

!start.

/* Plans */
+!start : verbose(V) <- 
    !startClient(V);
    ?region(R);
    !outR(lock(mutex), R).

{ include("tusow_agent.asl")}