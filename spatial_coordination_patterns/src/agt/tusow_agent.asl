// Agent tusow_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
tusow_base_name("tusow_").
/*
 * tusow_url("http://localhost:8080").
 * tupla(msg("aa", 0), region("cas_di_turi")).
 * tupleSpaceName("test").
*/
/* Initial goals */

//!startClient(true).

/* Plans */ 

+!startClient(Verbose) : tusow_url(Address) <- 
	!setVerbose(Verbose);
	!log("start ", Address);
	?tusow_base_name(BaseName);
   .my_name(Me);
   .concat(BaseName,Me,ArtifactName);
	makeArtifact(ArtifactName,"tusow.TuSoWClient",[Address],Id);
	!log("focus: ", Id);
	focus(Id);
	+tusow_artifact(Id).
	
+!outR(C, R) : true <- 
	!out(C, region(R)).
    
+!out(C, R) : tupleSpaceName(TSN) <-
	!log("Singole out: ", C, ", ", R);
	?tusow_artifact(Id);
	out(TSN, tupla(C,R)) [artifact_id(Id)];
	!log("request sended").

@planIn0 [atomic]
+!in(Template, Regions): tupleSpaceName(TSN) <- 
	!log("try in no result with template: ", Template, " and regions: ", Regions);
	?tusow_artifact(Id);
	in(TSN, Template, Regions) [artifact_id(Id)].
	
@planIn1 [atomic]
+!in(Template, Regions, Result): tupleSpaceName(TSN) <- 
	!log("try in with one result with template: ", Template, " and regions: ", Regions);
	?tusow_artifact(Id);
	in(TSN, Template, Regions, Result) [artifact_id(Id)].
	
@planIn2 [atomic]
+!in(Template, Regions, Result1, Result2): tupleSpaceName(TSN) <- 
	!log("try in with two result with template: ", Template, " and regions: ", Regions);
	?tusow_artifact(Id);
	in(TSN, Template, Regions, Result1, Result2) [artifact_id(Id)].
	
@planRd0 [atomic]
+!rd(Template, Regions): tupleSpaceName(TSN) <- 
	!log("try rd no result with template: ", Template, " and regions: ", Regions);
	?tusow_artifact(Id);
	rd(TSN, Template, Regions) [artifact_id(Id)].
	
@planRd1 [atomic]
+!rd(Template, Regions, Result): tupleSpaceName(TSN) <- 
	!log("try rd with one result with template: ", Template, " and regions: ", Regions);
	?tusow_artifact(Id);
	rd(TSN, Template, Regions, Result) [artifact_id(Id)].
	
@planRd2 [atomic]
+!rd(Template, Regions, Result1, Result2): tupleSpaceName(TSN) <- 
	!log("try rd with two result with template: ", Template, " and regions: ", Regions);
	?tusow_artifact(Id);
	rd(TSN, Template, Regions, Result1, Result2) [artifact_id(Id)].

+!log(M1, M2, M3, M4) : true <- 
	.concat(M1, M2, M);
	!log(M, M3, M4).

+!log(M1, M2, M3) : true <- 
	.concat(M1, M2, M);
	!log(M, M3).	

+!log(M1, M2) : true <- .concat(M1, M2, M); !log(M).

+!log(Msg) : verbose(true) <- .print(Msg).

-!log(Msg).

+!setVerbose(T) : true <-
	.concat("set verbose to ", T, M); 
	.print(M);
	-+verbose(T).

-!setVerbose(T).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
