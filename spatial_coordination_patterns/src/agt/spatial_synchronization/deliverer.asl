/* Initial beliefs and rules */

// tusow
tusow_url("http://localhost:8080").
tupleSpaceName("spatial_synchronization").

// synapsis
synapsis_url("ws://localhost:9000/").
synapsis_mind_class("spatial_synchronization.DelivererMind").
reconnection_attempts(5).

deliveringPlace("region-store-Sphere-80").

/* Initial goals */

!start(true).

/* Plans */

+synapsis_counterpart_status(Name, C): .my_name(Me) & .substring(Me,Name) <-
    ?my_synapsis_mind_ID(MyArtID);
    if (C == true){
       synapsisLog("Body connected")[artifact_id(MyArtID)];
       !reachDeliveringPlace;
    } else {
       .drop_all_intentions;
       synapsisLog("Body not connected")[artifact_id(MyArtID)];
    }.

+deliverPackage <-
    // generates a random package code
    generatePackageCodeAction;
    // simulates a delivering time
    !waitRandomly;
    // retrieves the generated package code
    ?deliveredPackageCode(C);
    // retrieves the delivering address address
    ?deliveringPlace(R);
    // update TuSoW with the tuple
    !out(delivered(package(C,"toys")), region(R));
    // removes the belief
    -deliverPackage;
    // waits for a retriever to be in the delivering place
    .print("waiting for a retriever to be in the delivering place");
    !rd(hereIAm(Deliverer), [R], Result);
    .print("leaving delivering place");
    leaveDeliveringPlaceAction(R).

+!start(Verbose) <-
    !startClient(Verbose);
    !createSynapsisMind([]).

+!reachDeliveringPlace <-
    ?deliveringPlace(R);
	goToDeliveringPlaceAction(R).

+!waitRandomly <-
    .random(R);
    .wait(R * 3000).

{ include("tusow_agent.asl") }
{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl") }
