/* Initial beliefs and rules */

// tusow
tusow_url("http://localhost:8080").
tupleSpaceName("spatial_synchronization").

// synapsis
synapsis_url("ws://localhost:9000/").
synapsis_mind_class("spatial_synchronization.RetrieverMind").
reconnection_attempts(5).

deliveringPlace("region-store-Sphere-80").

/* Initial goals */

!start(true).

/* Plans */

+synapsis_counterpart_status(Name, C): .my_name(Me) & .substring(Me,Name) <-
    ?my_synapsis_mind_ID(MyArtID);
    if (C == true){
       synapsisLog("Body connected")[artifact_id(MyArtID)];
       !reachDeliveringPlace;
    } else {
       .drop_all_intentions;
       synapsisLog("Body not connected")[artifact_id(MyArtID)];
    }.

+arrivedToDeliveringPlace <-
    ?deliveringPlace(R);
    // notifies the arrival of the retriever to the delivering place
    !out(hereIAm("retriever"), region(R));
    !takePackage.

+!start(Verbose) <-
    !startClient(Verbose);
    !createSynapsisMind([]).

+!reachDeliveringPlace <-
    ?deliveringPlace(R);
	goToDeliveringPlaceAction(R).

+!takePackage <-
    ?deliveringPlace(R);
    // waiting for the delivering of a package at region R
    !in(delivered(package(Code, Desc)), [R], ResCode, ResDesc);
    !log("Took package with code: ", ResCode, " and description: ", ResDesc).

{ include("tusow_agent.asl") }
{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl") }
