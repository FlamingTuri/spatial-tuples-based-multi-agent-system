/* Initial beliefs and rules */

// tusow
tusow_url("http://localhost:8080").
tupleSpaceName("follow_me").

// synapsis
synapsis_url("ws://localhost:9000/").
synapsis_mind_class("follow_me.HanselMind").
reconnection_attempts(5).

counter(0).
distance(100).
directionOffset(40).

/* Initial goals */

!start(true).

/* Plans */

+synapsis_counterpart_status(Name, C): .my_name(Me) & .substring(Me,Name) <-
    ?my_synapsis_mind_ID(MyArtID);
    if (C == true) {
       synapsisLog("Body connected")[artifact_id(MyArtID)];
       +putBreadcrumb;
    } else {
       .drop_all_intentions;
       synapsisLog("Body not connected")[artifact_id(MyArtID)];
    }.

+putBreadcrumb <-
    ?counter(C);
    .print("sending body notification to put breadcrumb");
    // remove observable property
    -putBreadcrumb;
    // send message to middleware to create a new region for the breadcrumb
    putBreadcrumbAction(C).

+addBreadcrumbTuple(R) <-
    ?counter(C);
    !out(wasHere("hansel",C), region(R));
    // increase the breadcrumb counter
    -+counter(C+1);
    // remove observable property
    -addBreadcrumbTuple(R);
    !move.

+!start(Verbose) <-
    !startClient(Verbose);
    !createSynapsisMind([]).

+!move <-
    ?distance(D);
    ?directionOffset(O);
    // move for some time
    moveAction(D, O).

{ include("tusow_agent.asl") }
{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl") }
