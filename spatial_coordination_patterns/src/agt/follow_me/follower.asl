/* Initial beliefs and rules */

// tusow
tusow_url("http://localhost:8080").
tupleSpaceName("follow_me").

// synapsis
synapsis_url("ws://localhost:9000/").
synapsis_mind_class("follow_me.FollowerMind").
reconnection_attempts(5).

counter(0).

/* Initial goals */

!start(true).

/* Plans */

+searchBreadcrumb: true <-
    ?counter(C);
    // seatch region where is located the next breadcrumb
    !rd(wasHere("hansel", C), [R], Result);
    .print("risultato");
    .print(Result);
    // increase the breadcrumb counter to search
    -+counter(C+1);
    // remove belief
    -searchBreadcrumb;
    // search the next breadcrumb
    !goToBreadcrumb(Result).

+synapsis_counterpart_status(Name, C): .my_name(Me) & .substring(Me,Name) <-
    ?my_synapsis_mind_ID(MyArtID);
    if (C == true) {
       synapsisLog("Body connected")[artifact_id(MyArtID)];
       +searchBreadcrumb;
    } else {
       .drop_all_intentions;
       synapsisLog("Body not connected")[artifact_id(MyArtID)];
    }.

+!start(Verbose) <-
    !startClient(Verbose);
    !createSynapsisMind([]).

+!goToBreadcrumb(R) <-
    ?counter(C);
    !log("going to breadcrumb #", C-1);
	goToBreadcrumbAction(R).

{ include("tusow_agent.asl") }
{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl") }
