// Agent sample_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
tusow_url("http://localhost:8080").
tupleSpaceName("find_me").
expetedDevice(5).
region("region-meetingPlace-Cube-80").
verbose(true).

/* Initial goals */

!start.

/* Plans */
+!start : verbose(V) <- 
    !startClient(V);
    !waitDevice.
    

+!start : true <-
	+verbose(false);
	!start.

+!waitDevice : expetedDevice(N) & N > 0 <-
	.print("wait device");
	?region(R);
	!in(hereIAm(Device), [R], Device);
	.print("arrived device: ", Device);
	-+expetedDevice((N-1));
	!waitDevice.
	
+!waitDevice : expetedDevice(N) & N == 0 <-
	.print("all device are arived").

{ include("tusow_agent.asl")}