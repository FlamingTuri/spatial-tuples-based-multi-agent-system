// Agent sample_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
/*tusow */
tusow_url("http://localhost:8080").
tupleSpaceName("find_me").
region("region-meetingPlace-Cube-80").
verbose(true).
/*synapsis */
synapsis_url("ws://localhost:9000/").
synapsis_mind_class("findme.DeviceMind").
reconnection_attempts(5).


pendingJob.
/* Initial goals */

!start.

/* Plans */

+arrivedTo(R) : region(Reg) & R == Reg <-
	.my_name(ID);
	.print(ID, "arrived to region");
	!outR(hereIAm(ID), R).

+!start : verbose(V) <- 
    !startClient(V);
    !createSynapsisMind([]).

+!start : true <-
	+verbose(false);
	!start.
	
+synapsis_counterpart_status(Name, C): pendingJob & C & .my_name(Me) & .substring(Me,Name) <-
    !log("Body connected");
    !goToRegion.
    
+synapsis_counterpart_status(Name, C): not C & .my_name(Me) & .substring(Me,Name) <-
    .drop_all_intentions;
    !log("Body not connected").
    
-synapsis_counterpart_status(Name, C): true <- !log("no pending job").
	
+!goToRegion : region(R)<-
	.print("go to region: ", R);
	?my_synapsis_mind_ID(MyArtID);
	goToRegion(R)[artifact_id(MyArtID)].

{ include("tusow_agent.asl")}
{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl")}