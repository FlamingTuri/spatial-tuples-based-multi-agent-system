// Agent sample_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
tusow_url("http://localhost:8080").
tupleSpaceName("test").

/* Initial goals */

!start.

/* Plans */

+!start : true <-
    !startClient(true);
    .wait(10000);
    !in(msg(X, Y), ["cas_di_pla", "cas_di_turi"], X, Y);
	.print("in result: ", X, " ", Y).

{ include("tusow_agent.asl") }
