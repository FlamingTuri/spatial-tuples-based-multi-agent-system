/* Initial beliefs and rules */

/* Initial goals */

/* Beliefs dinamici */

+synapsis_counterpart_status(Name, C): .my_name(Me) & .substring(Me,Name) <-
    ?my_synapsis_mind_ID(MyArtID);
    if (C == true){
       synapsisLog("Controparte collegata")[artifact_id(MyArtID)];
    } else {
       .drop_all_intentions;
       synapsisLog("Controparte non collegata")[artifact_id(MyArtID)];
    }.

{ include("jar:file:lib/SynapsisJaCaMo.jar!/agt/synapsis/synapsis_base_agent.asl") }

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
