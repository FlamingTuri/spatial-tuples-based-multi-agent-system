// Agent sample_agent in project spatial_coordination_patterns

/* Initial beliefs and rules */
tusow_url("http://localhost:8080").
tupla(msg("aa", 0), region("cas_di_turi")).
tupleSpaceName("test").

/* Initial goals */

!start.

/* Plans */

+out(T): true <-
	!log("out ", T).

+!start : true <-
    !startClient(true);
    .wait(2000);
	?tupla(C, R);
	!out(C,R).

{ include("tusow_agent.asl") }
