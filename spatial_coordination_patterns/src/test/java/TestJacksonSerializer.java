import com.fasterxml.jackson.databind.ObjectMapper;
import models.Response;
import models.Term;
import models.Tuple;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestJacksonSerializer {

    @Test
    public void testTupleSerializer() {
        var tuple = new Tuple("msg");
        tuple.getArgs()
            .add(0)
            .add("something")
            .add(Term.createVarTerm("X"));

        var jsonTuple = tuple.toJsonString();

        try {
            var tupleFromJson = Tuple.fromJsonString(jsonTuple);
            assertEquals(jsonTuple, tupleFromJson.toJsonString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testResponseSerializer() {
        try {
            var jsonTemplate = "{\"fun\":\"msg\",\"args\":[\"a\",0,{\"var\": \"X\"}]}";
            var template = Tuple.fromJsonString(jsonTemplate);

            var jsonTuple = "{\"fun\":\"msg\",\"args\":[\"a\",0,{\"val\": \"a\"}]}";
            var tuple = Tuple.fromJsonString(jsonTuple);

            var objectMapper = new ObjectMapper();
            var map = objectMapper.createObjectNode()
                .put("key", "X");

            var response = new Response(template, tuple, true, map);

            var jsonResponse = response.toJsonString();

            var responseFromJson = Response.fromJsonString(jsonResponse);
            assertEquals(jsonResponse, responseFromJson.toJsonString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
