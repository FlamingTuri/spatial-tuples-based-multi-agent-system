import httpclient.HttpClientWrapper;

import models.Response;
import models.Term;
import models.Tuple;
import utils.JsonUtils;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTusow {

    @Test
    @Tag("slow")
    public void testApi() {
        var httpClient = new HttpClientWrapper();

        var tupleSpace = "custom";
        
        var a = new Tuple("a");
        a.getArgs().add("a");
        
        var content = new Tuple("msg");
        content.getArgs()
            .add(0)
            .add("something")
            .add(a.asJsonNode());
        
        var region = new Tuple("reg");
        region.getArgs()
        	.add("casa_di_pla");
        
        var tupla = new Tuple("tuple");
        tupla.getArgs()
        	.add(content.asJsonNode())
        	.add(region.asJsonNode());

        var tupleToString = tupla.toJsonString();
        System.out.println(tupleToString);
        httpClient.post(tupleSpace, tupleToString)
            .thenAccept(httpResponse -> {
                try {
                    var tuples = Tuple.fromJsonArrayString(httpResponse.body());
                    assertEquals(tupleToString, tuples[0].toJsonString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).join();

        var tupleTemplate = new Tuple("tuple");
        tupleTemplate.getArgs()
            .add(Term.createVarTerm("X"))
            .add(Term.createVarTerm("Y"));
        httpClient.get(tupleSpace, tupleTemplate.toJsonString())
            .thenAccept(httpResponse -> {
                try {
                    var response = Response.fromJsonArrayString(httpResponse.body());
                    Arrays.stream(response).map(JsonUtils::toJsonString).forEach(System.out::println);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).join();
    }
}
