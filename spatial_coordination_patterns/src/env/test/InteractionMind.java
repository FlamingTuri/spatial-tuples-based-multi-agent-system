package test;

import synapsis.SynapsisMind;
import utils.Logger;

import java.util.ArrayList;

public class InteractionMind extends SynapsisMind {

    private static final String ROBOT_TYPE = "robot_type";

    protected void init(final String agentName, final String url, final int reconnectionAttempts,
            final Object[] params) {
        Logger.print(agentName, url, reconnectionAttempts);
        super.init(agentName, url, reconnectionAttempts);
    }

    @Override
    public void counterpartEntityReady() {
        Logger.print("body is ready");
    }

    @Override
    public void counterpartEntityUnready() {
        Logger.print("body is not ready");
    }

    @Override
    public void parseIncomingPerception(String content, ArrayList<Object> params) {
        Logger.print("parsing incoming perception");
        Logger.print("content: " + content);
        params.stream().map(p -> (String) p).forEach(Logger::print);

        doAction("do something my slave", "param1", "param2");
    }

}
