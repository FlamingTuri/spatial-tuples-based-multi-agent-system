package spatial_synchronization;

import synapsis.SynapsisMind;
import utils.Logger;

import java.util.ArrayList;

import cartago.OPERATION;
import constants.Actions;
import constants.Perceptions;

public class RetrieverMind extends SynapsisMind {

    private static final String ARRIVED_TO_DELIVERING_PLACE_BELIEF = "arrivedToDeliveringPlace";

    protected void init(final String agentName, final String url, final int reconnectionAttempts,
            final Object[] params) {
        Logger.print(agentName, url, reconnectionAttempts);
        super.init(agentName, url, reconnectionAttempts);
    }

    @Override
    public void counterpartEntityReady() {
        Logger.print("body is ready");
    }

    @Override
    public void counterpartEntityUnready() {
        Logger.print("body is not ready");
    }

    @Override
    public void parseIncomingPerception(String content, ArrayList<Object> params) {
        switch (content) {
        case Perceptions.ARRIVED_INSIDE_REGION:
            defineObsProperty(ARRIVED_TO_DELIVERING_PLACE_BELIEF);
            break;
        default:
            Logger.print("Unhandled perception type: " + content);
            if (params.size() > 0) {
                Logger.print("With params: ");
                params.stream().map(p -> (String) p).forEach(Logger::print);
            }
        }
    }

    @OPERATION
    public void goToDeliveringPlaceAction(String deliveringPlaceRegion) {
        doAction(Actions.GOTO_REGION, deliveringPlaceRegion);
    }
}
