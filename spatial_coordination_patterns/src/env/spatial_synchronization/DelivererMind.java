package spatial_synchronization;

import java.util.Random;
import synapsis.SynapsisMind;
import utils.Logger;

import java.util.ArrayList;

import cartago.OPERATION;
import constants.Actions;
import constants.Perceptions;

public class DelivererMind extends SynapsisMind {

    private static final String DELIVER_PACKAGE_BELIEF = "deliverPackage";
    private static final String DELIVER_PACKAGE_CODE_BELIEF = "deliveredPackageCode";
    private static final double PACKAGE_RADIUS = 5.0;

    protected void init(final String agentName, final String url, final int reconnectionAttempts,
            final Object[] params) {
        Logger.print(agentName, url, reconnectionAttempts);
        super.init(agentName, url, reconnectionAttempts);
    }

    @Override
    public void counterpartEntityReady() {
        Logger.print("body is ready");
    }

    @Override
    public void counterpartEntityUnready() {
        Logger.print("body is not ready");
    }

    @Override
    public void parseIncomingPerception(String content, ArrayList<Object> params) {
        switch (content) {
        case Perceptions.ARRIVED_INSIDE_REGION:
            defineObsProperty(DELIVER_PACKAGE_BELIEF);
            break;
        default:
            Logger.print("Unhandled perception type: " + content);
            if (params.size() > 0) {
                Logger.print("With params: ");
                params.stream().map(p -> (String) p).forEach(Logger::print);
            }
        }
    }

    @OPERATION
    public void goToDeliveringPlaceAction(String deliveringPlaceRegion) {
        doAction(Actions.GOTO_REGION, deliveringPlaceRegion);
    }

    @OPERATION
    public void generatePackageCodeAction() {
        int randomCode = new Random().nextInt(90000) + 10000;
        String packageCode = "code-" + randomCode;
        defineObsProperty(DELIVER_PACKAGE_CODE_BELIEF, packageCode);
    }

    @OPERATION
    public void leaveDeliveringPlaceAction(String deliveringPlaceRegion) {
        doAction(Actions.LEAVE_REGION, deliveringPlaceRegion);
    }

}
