// CArtAgO artifact code for project spatial_coordination_patterns

package tusow;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import cartago.Artifact;
import cartago.GUARD;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import httpclient.HttpClientWrapper;
import models.Response;
import models.Tuple;
import utils.TupleParser;


public class TuSoWClient extends Artifact {

	private HttpClientWrapper client;
	private long countQuery = 0L;
	private List<Long> queryEnded = new LinkedList<>();

	void init(String address) {
		defineObsProperty("address", address);
		client = new HttpClientWrapper(address);
	}

	@OPERATION
	void out(String tupleSpace, String tupla) {
		tupla = tupla.replace("\"", "");
		log("request out to: " + tupleSpace + "for tupla: " + tupla);

		//TODO parse to TusowTupla
		Tuple tusowTupla = TupleParser.parseCartagoTupleToTusowTuple(tupla);
		//TODO make request
		client.post(tupleSpace, tusowTupla.toJsonString())
		.thenAccept(response -> {
			//TODO when response add tupla / or await response and return fail or id response and add tupla to belief
			beginExternalSession();
			notifyResult(response.statusCode(), response.body());
			endExternalSession(true);
		});
	}

	@OPERATION
	void in(String tupleSpace, String tupla, Object region, Object... feed) {
		in(tupleSpace, tupla, new Object[] {region}, feed);
	}

	@SuppressWarnings("unchecked") //is checked in the first if
	@OPERATION
	void in(String tupleSpace, String templateContent, Object[] regions, Object... feed) {
		if (Stream.of(feed).anyMatch(p -> !(p instanceof OpFeedbackParam<?>))) {
			throw new IllegalArgumentException("at least one of the output parameters is not an istance of OpFeedbackParam<?>");
		}
		List<Response> res = inBlocking(tupleSpace, templateContent, regions);
		if (res.isEmpty() || !res.get(0).isMatch()) {
			throw new IllegalStateException("Here must be a response");
		}
		setResponseVariable(res.get(0), Stream.of(feed).map(m -> (OpFeedbackParam<String>)m).collect(Collectors.toList()));
	}

	@OPERATION
	void rd(String tupleSpace, String tupla, Object region, Object... feed) {
		rd(tupleSpace, tupla, new Object[] {region}, feed);
	}

	@SuppressWarnings("unchecked") //is checked in the first if
	@OPERATION
	void rd(String tupleSpace, String templateContent, Object[] regions, Object... feed) {
		if (Stream.of(feed).anyMatch(p -> !(p instanceof OpFeedbackParam<?>))) {
			throw new IllegalArgumentException("at least one of the output parameters is not an istance of OpFeedbackParam<?>");
		}
		List<Response> res = rdBlocking(tupleSpace, templateContent, regions);
		if (res.isEmpty() || !res.get(0).isMatch()) {
			throw new IllegalStateException("Here must be a response");
		}
		setResponseVariable(res.get(0), Stream.of(feed).map(m -> (OpFeedbackParam<String>)m).collect(Collectors.toList()));
	}

	private void setResponseVariable(Response response, List<OpFeedbackParam<String>> feedbackParams) {
		if (response == null) {
			throw new IllegalStateException("Here must be a response");
		}
		var mapping = response.getMap();
		if (mapping.size() != feedbackParams.size()) {
			throw new IllegalStateException("Wrong number of mapping variable-value, actual: " + mapping.size() + ", expetted: " + feedbackParams.size());
		}
		var templateMatched = response.getTemplate().toJsonString();
		var error = iteratorToStream(mapping.fields()).map(m -> m.getKey()).filter(p -> !templateMatched.contains(p)).findFirst();
		if (error.isPresent()) {
			throw new IllegalStateException("variable name: " + error + " not present in template: " + templateMatched);
		}
		var mappingOrdered = iteratorToStream(mapping.fields())
				.sorted((m1,m2) -> Integer.compare(templateMatched.indexOf(m1.getKey()), templateMatched.indexOf(m2.getKey())))
				.collect(Collectors.toList());
		for (int i = 0; i < mappingOrdered.size(); i++) {
			feedbackParams.get(i).set(mappingOrdered.get(i).getValue().asText());
		}
	}

	private <T> Stream<T> iteratorToStream(Iterator<T> it) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(it, Spliterator.ORDERED), false);
	}

	private List<Response> inBlocking(String tupleSpace, String templateContent, Object[] regionsArray) {
		return blockingRequest(tupleSpace, templateContent, regionsArray, true);
	}

	private List<Response> rdBlocking(String tupleSpace, String templateContent, Object[] regionsArray) {
		return blockingRequest(tupleSpace, templateContent, regionsArray, false);
	}
	
	private String regionToString(Object region) {
		String prefix = region instanceof String ? "" : "_";
		return prefix + region.toString();
	}

	private List<Response> blockingRequest(String tupleSpace, String templateContent, Object[] regionsArray, boolean isIn) {
		templateContent = templateContent.replace("\"", "");
		if (regionsArray.length > 0 && !( regionsArray[0] instanceof String)) {
			regionsArray = (Object[]) regionsArray[0];
		}
		String regions = Stream.of(regionsArray).map(m -> regionToString(m)).reduce((a,b) -> a + ", " + b).orElse("");
		log("request in to: " + tupleSpace + " for tupla: " + templateContent + " in regions: " + regions);
		var tusowContentTemplate = TupleParser.parseCartagoTupleToTusowTuple(templateContent);
		var query = "[" +
			Stream.of(regionsArray)
			.map(r -> "region(" + regionToString(r) + ")")
			.map(r -> TupleParser.parseCartagoTupleToTusowTuple(r))
			.map(r -> {
				var tupla = new Tuple("tupla");
				tupla.getArgs()
					.add(tusowContentTemplate.asJsonNode())
					.add(r.asJsonNode());
				return tupla.toJsonString();
			})
			.reduce((a, b) -> a + ", " + b)
			.orElse("") + "]";
		log("query: " + query);
		//generate uuid
		long queryId = countQuery++;
		final List<Response> responses = new LinkedList<>();
		var request = isIn ? client.delete(tupleSpace, query) : client.get(tupleSpace, query);
        request.thenAccept(httpResponse -> {
            try {
            	beginExternalSession();
                Stream.of(Response.fromJsonArrayString(httpResponse.body())).forEach(res -> responses.add(res));
                queryEnded.add(queryId);
                endExternalSession(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
		//await guard
		await("awaitIn", queryId);
		queryEnded.remove(queryId);
		return responses;
	}

	@GUARD
	boolean awaitIn(long id) {
		return queryEnded.contains(id);
	}

	void notifyResult(int statusCode, String body) {
		signal("code", statusCode);
		defineObsProperty("out", body);
	}
}
