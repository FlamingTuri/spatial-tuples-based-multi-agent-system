package constants;

public class Actions {

    public static final String CREATE_REGION = "createRegion";
    public static final String GET_CURRENT_POSITION = "getCurrentPosition";
    public static final String GET_CURRENT_REGIONS = "getCurrentRegions";
    public static final String GOTO_POSITION = "gotoPosition";
    public static final String GOTO_REGION = "gotoRegion";
    public static final String LEAVE_REGION = "leaveRegions";
    public static final String MOVE = "move";
}
