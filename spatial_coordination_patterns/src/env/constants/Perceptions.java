package constants;

public class Perceptions {

    public static final String REGION_CREATED = "regionCreated";
    public static final String CURRENT_POSITION = "currentPosition";
    public static final String CURRENT_REGIONS = "currentRegions";

    public static final String REGION_CREATION_FAILED = "regionCreationFailed";
    public static final String ARRIVED_TO_POSITION = "arrivedToPosition";
    public static final String ARRIVED_TO_REGION = "arrivedToRegion";
    public static final String MOVE_COMPLETED = "moveCompleted";
    public static final String ARRIVED_INSIDE_REGION = "arrivedInsideRegion";
    public static final String ARRIVED_NEAR_REGION = "arrivedNearRegion";
    public static final String EXITED_FROM_REGION = "exitedFromRegions";
    public static final String ENTERED_REGION = "enteredRegion";
    public static final String LEFT_REGION = "leftRegion";
}
