package follow_me;

import synapsis.SynapsisMind;
import utils.Logger;

import java.util.ArrayList;

import cartago.OPERATION;
import constants.Actions;
import constants.Perceptions;

public class FollowerMind extends SynapsisMind {

    private static final String SEARCH_BREADCRUMB_BELIEF = "searchBreadcrumb";

    protected void init(final String agentName, final String url, final int reconnectionAttempts,
            final Object[] params) {
        Logger.print(agentName, url, reconnectionAttempts);
        super.init(agentName, url, reconnectionAttempts);
    }

    @Override
    public void counterpartEntityReady() {
        Logger.print("body is ready");
    }

    @Override
    public void counterpartEntityUnready() {
        Logger.print("body is not ready");
    }

    @Override
    public void parseIncomingPerception(String content, ArrayList<Object> params) {
        switch(content) {
        case Perceptions.ARRIVED_INSIDE_REGION:
            defineObsProperty(SEARCH_BREADCRUMB_BELIEF);
            break;
        default:
            Logger.print("Unhandled perception type: " + content);
        }
    }

    @OPERATION
    public void goToBreadcrumbAction(String breadcrumbRegion) {
        doAction(Actions.GOTO_REGION, breadcrumbRegion);
    }
}
