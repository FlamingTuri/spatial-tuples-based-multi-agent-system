package follow_me;

import synapsis.SynapsisMind;
import utils.Logger;

import java.util.ArrayList;

import cartago.OPERATION;
import constants.Actions;
import constants.Perceptions;
import constants.RegionShape;

public class HanselMind extends SynapsisMind {

    private static final String ADD_BREADCRUMB_TUPLE_BELIEF = "addBreadcrumbTuple";
    private static final String PUT_BREADCRUMB_BELIEF = "putBreadcrumb";
    private static final double BREADCRUMB_RADIUS = 5.0;

    protected void init(final String agentName, final String url, final int reconnectionAttempts,
            final Object[] params) {
        Logger.print(agentName, url, reconnectionAttempts);
        super.init(agentName, url, reconnectionAttempts);
    }

    @Override
    public void counterpartEntityReady() {
        Logger.print("body is ready");
    }

    @Override
    public void counterpartEntityUnready() {
        Logger.print("body is not ready");
    }

    @Override
    public void parseIncomingPerception(String content, ArrayList<Object> params) {
        switch (content) {
        case Perceptions.REGION_CREATED:
            defineObsProperty(ADD_BREADCRUMB_TUPLE_BELIEF, params.get(0));
            break;
        case Perceptions.MOVE_COMPLETED:
            defineObsProperty(PUT_BREADCRUMB_BELIEF);
            break;
        default:
            Logger.print("Unhandled perception type: " + content);
        }
    }

    @OPERATION
    public void putBreadcrumbAction(int counter) {
        doAction(Actions.CREATE_REGION, Integer.toString(counter), RegionShape.SPHERE, BREADCRUMB_RADIUS);
    }

    @OPERATION
    public void moveAction(int distance) {
        doAction(Actions.MOVE, distance);
    }


    @OPERATION
    public void moveAction(int distance, int directionOffset) {
        doAction(Actions.MOVE, distance, directionOffset);
    }

}
