package mutex;

import constants.*;
import java.util.ArrayList;

import cartago.OPERATION;
import synapsis.SynapsisMind;
import utils.Logger;

public class AgentMind extends SynapsisMind {
	
	private static final String BELIEF_ARRIVED_NEAR = "arrivedNear";
	private static final String BELIEF_ARRIVED_INSIDE = "arrivedInside";
	private static final String BELIEF_EXIT_FROM = "exitFrom";

	protected void init(final String agentName, final String url, final int reconnectionAttempts,
            final Object[] params) {
        Logger.print(agentName, url, reconnectionAttempts);
        super.init(agentName, url, reconnectionAttempts);
    }

    @Override
    public void counterpartEntityReady() {
        Logger.print("body is ready");
    }

    @Override
    public void counterpartEntityUnready() {
        Logger.print("body is not ready");
    }

    @Override
    public void parseIncomingPerception(String content, ArrayList<Object> params) {
        Logger.print("parsing incoming perception");
        Logger.print("content: " + content);
        params.stream().map(p -> (String) p).forEach(Logger::print);
        //TODO check if needed
        if (content.equals(Perceptions.ARRIVED_NEAR_REGION) && params.size() == 1) {
        	defineObsProperty(BELIEF_ARRIVED_NEAR, params.get(0));
        }
        if (content.equals(Perceptions.ARRIVED_INSIDE_REGION) && params.size() == 1) {
        	defineObsProperty(BELIEF_ARRIVED_INSIDE, params.get(0));
        }
        if (content.equals(Perceptions.EXITED_FROM_REGION) && params.size() == 1) {
        	defineObsProperty(BELIEF_EXIT_FROM, params.get(0));
        }
    }
    
    @OPERATION
	void goNearRegion(String region) {
		doAction(Actions.GOTO_REGION, region, true);
	}
    
    @OPERATION
	void goInsideRegion(String region) {
		doAction(Actions.GOTO_REGION, region, false);
	}
    
    @OPERATION
	void leaveRegion(String region) {
		doAction(Actions.LEAVE_REGION, region);
	}


}
